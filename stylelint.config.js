module.exports = {
  extends: ["stylelint-config-airbnb"],
  rules: {
    "declaration-property-value-blacklist": null,
    "rule-empty-line-before": null,
    "max-nesting-depth": 3,
    "comment-empty-line-before": null
  }
};
