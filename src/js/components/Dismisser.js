const Dismisser = () => {
  const elements = document.querySelectorAll('[data-dismiss]')

  for (let i = 0; i < elements.length; i += 1) {
    elements[i].onclick = () => {
      if (elements[i].parentNode.classList.contains(elements[i].dataset.dismiss)) {
        elements[i].parentNode.parentNode.removeChild(elements[i].parentNode)
      }
    }
  }
}

export default Dismisser;
